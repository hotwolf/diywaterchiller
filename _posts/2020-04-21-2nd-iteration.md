---
layout: post
title:  2nd Iteration
---

I've built a second iteration of my Peltier water cooler. This time it's a more modular approach with a radiator and fans right on the board.

![2nd iteration of my cooler]({{ site.url }}/images/2nd_chiller_1.jpg)

My third set of cooling blocks is still on it's way and one of the four Peltier elemnts turns out to be broken. So with thee working TEC1-12706 elements
I'm able to cool the water flow down by about 0.5 &#8451;.

So now I've moved the old radiators into the primary cooling circuit, inbetween the outlet of the laser tube and the water tank.

![2nd iteration of my cooler]({{ site.url }}/images/2nd_chiller_2.jpg)

This setup does not leak any water, but it does somehow leak quite a bit of air into the system. Luckily my water pump is strong enough to push out the air
within a few seconds.
