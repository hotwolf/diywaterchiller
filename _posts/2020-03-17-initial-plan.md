---
layout: post
title:  Initial Plan
---

My initial plan to cool the laser tube was to simply use a large tank full of destilled water at ambient temperature. An UV bulb insude the tank was supposed to keep the water clean. And to protect the tank walls from the UV light, they were to be coated with adhesive aluminium foil.

![aluminium coated tank]({{ site.url }}/images/tub.jpg)

Unfortunately is not enough. The laser tube is to be operated at 15&deg;C to 20&deg;C, but my basement already has an ambient temperature of 20&deg;C. I need an active cooling system.
