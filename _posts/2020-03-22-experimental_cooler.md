---
layout: post
title:  Experimental Cooler
---

My Peltier elements have arrived in the mail, so I rigged up an experimental cooler setup without any current regulation. I'm using four 40x80mm aluminium water coolers and four TEC1-12706 peltier elements. Two aluminium blocks are now part of the primary cooling circuit and are sandwiched by the two aluminium blocks of the secondary circuit.

![experimental cooler]({{ site.url }}/images/experimental_cooler.jpg)

The secondary cooling circuit runs through some radiators, which are atteched to my exhaust fan.

![radiator]({{ site.url }}/images/radiator.jpg)

This contraption works to some extend. I defenitely don't have to worry about regulating the current that runs through the peltier elements. The cooling effect is better than nothing, but the usability of this installation needs improvement.
