---
layout: post
title:  Chiller Mounts
---

Working with thermal paste is very messy. So I printed some mounts to encapsulate the aluminum blocks.
This way I don't have to touch them anymore and hopefully this will also provide some insulation for
the cold parts.

![chiller mounts]({{ site.url }}/images/chiller_bracket_1.jpg)

![chiller mount]({{ site.url }}/images/chiller_bracket_2.jpg)
